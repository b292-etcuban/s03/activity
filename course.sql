CREATE DATABASE course_db;

DROP DATABASE course_db;

CREATE DATABASE course_db;

USE course_db;

CREATE TABLE students(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	email VARCHAR(50),
	PRIMARY KEY (id)
);

CREATE TABLE subjects(
	id INT NOT NULL AUTO_INCREMENT,
	course_name VARCHAR(100) NOT NULL,
	schedule VARCHAR(100) NOT NULL,
	instructor VARCHAR(100) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE enrollments(
	id INT NOT NULL AUTO_INCREMENT,
	student_id INT NOT NULL,
	subject_id INT NOT NULL,
	datetime_created TIMESTAMP NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_enrollments_student_id
		FOREIGN KEY (student_id) REFERENCES enrollments(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_enrollments_subject_id
		FOREIGN KEY (subject_id) REFERENCES enrollments(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

SHOW TABLES;

INSERT INTO students(username, password, full_name, email) VALUES ('lulusupport', 'lulu', 'Lulu Yordle', 'lulu@school.com');

SELECT *FROM students;

INSERT INTO subjects(course_name, schedule, instructor) VALUES ('Sorcery', '5PM-10PM', 'Ms. Camille');

SELECT *FROM subjects;

INSERT INTO enrollments(student_id, subject_id, datetime_created) VALUES (1, 1, '2023-05-03 20:42:50');

SELECT *FROM enrollments;